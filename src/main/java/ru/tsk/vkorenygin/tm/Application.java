package ru.tsk.vkorenygin.tm;

import ru.tsk.vkorenygin.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}