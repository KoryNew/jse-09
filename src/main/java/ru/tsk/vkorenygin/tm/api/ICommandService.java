package ru.tsk.vkorenygin.tm.api;

import ru.tsk.vkorenygin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
