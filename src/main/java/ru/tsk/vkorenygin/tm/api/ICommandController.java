package ru.tsk.vkorenygin.tm.api;

public interface ICommandController {
    void showErrorArgument();

    void showErrorCommand();

    void showAbout();

    void showVersion();

    void showInfo();

    void showCommands();

    void showArguments();

    void showHelp();

    void exit();
}
