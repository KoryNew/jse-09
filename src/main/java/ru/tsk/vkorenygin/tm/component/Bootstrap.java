package ru.tsk.vkorenygin.tm.component;

import ru.tsk.vkorenygin.tm.api.ICommandController;
import ru.tsk.vkorenygin.tm.api.ICommandRepository;
import ru.tsk.vkorenygin.tm.api.ICommandService;
import ru.tsk.vkorenygin.tm.constant.ArgumentConst;
import ru.tsk.vkorenygin.tm.constant.CommandConst;
import ru.tsk.vkorenygin.tm.controller.CommandController;
import ru.tsk.vkorenygin.tm.repository.CommandRepository;
import ru.tsk.vkorenygin.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {
    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        System.out.println("-- WELCOME TO TASK MANAGER --");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0)
            return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void parseCommand(String command) {
        switch (command) {
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

}
